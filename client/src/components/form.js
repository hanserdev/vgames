import React, { Component } from 'react';
import './form.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from 'axios';
import swal from 'sweetalert';


const styles = theme => ({
  root: {
    overflow: 'hidden',
    padding: `0 ${theme.spacing.unit * 3}px`,
  },
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: `${theme.spacing.unit * 3}px`,
  },
  wrapper: {
    maxWidth: 400,
  },
  paper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit * 2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  table: {
    minWidth: 700,
  },
});

function validar_email( email ) {
    const regex = (/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    return regex.test(email) ? true : false;
}


class Form extends Component{

  constructor(){
    super();
    this.state = {
      nombre: '',
      apellidos: '',
      correo: '',
      password: '',
      error: {
        password: false,
        correo: false
      },
      user: []
    }
    this.handleInput = this.handleInput.bind(this);
    this.handleForm = this.handleForm.bind(this);
    this.getUsers = this.getUsers.bind(this)
  }

  async componentWillMount() {
    let item = await this.getUsers()
    console.log(item);
    this.setState({
      user: item.res
    })
  }

  getUsers(){
    return new Promise(function(resolve, reject) {
       axios.get('http://localhost:3003/home/datos')
       .then((res) => {
         console.log(res);
         resolve(res.data)
       })
    });
  }

  handleInput(i){
    const {value, name} = i.target
    this.setState({
      [name]: value
    })
  }

  handleForm(e) {
    e.preventDefault();
    const d = this.state
    if (!validar_email(d.correo)) {
      this.setState({
        error: {
          correo: true
        }
      })
    }else if (d.password.length < 8) {
      this.setState({
        error: {
          password: true,
          correo: false
        }
      })
    }else {
      this.setState({
        error: {
          password: false,
          correo: false
        }
      })

      axios.post('http://localhost:3003/home/enviar_datos', {
        nombre: d.nombre,
        apellidos: d.apellidos,
        correo: d.correo,
        password: d.password
      })
      .then(res => {
        console.log(res.data);
        swal({
          title: "Los datos fueron enviados correctamente!",
          icon: "success",
        })
        .then((willDelete) => {
          window.location.reload();
        })
      })
    }
  }

  render(){

    const { classes } = this.props;
    return (
      <div className="Form">

      <div id="grid" className={classes.root}>

      <Paper className={classes.paper}>
      <Grid container wrap="nowrap" spacing={16}>
        <Grid item xs={4}>
          <Typography variant="h4" noWrap>Formulario de envio de datos:</Typography>
          <form onSubmit={this.handleForm}>
            <TextField
              label="Nombre"
              placeholder="Ej. Jesus"
              name="nombre"
              onChange={this.handleInput}
              className={classes.textField}
              style={{ width: 450, top: 10 }}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            /><br></br>

            <TextField
              label="Apellidos"
              placeholder="Ej. Fuentes Alaves"
              name="apellidos"
              onChange={this.handleInput}
              className={classes.textField}
              style={{ width: 450, top: 10 }}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            /><br></br>

            <TextField
              error={this.state.error.correo}
              label="Correo electronico"
              placeholder="Ej. correo@mail.com"
              name="correo"
              onChange={this.handleInput}
              className={classes.textField}
              style={{ width: 450, top: 10 }}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            /><br></br>

            <TextField
              error={this.state.error.password}
              type="password"
              label="Contraseña"
              placeholder="Contraseña..."
              name="password"
              helperText="8 caracteres"
              onChange={this.handleInput}
              className={classes.textField}
              style={{ width: 450, top: 10 }}
              InputLabelProps={{
                shrink: true,
              }}
              margin="normal"
            /><br></br>

            <Button type="submit" value="Submit" id="buttonSend" variant="contained" color="primary" disabled={!this.state.nombre || !this.state.apellidos || !this.state.correo || !this.state.password} className={classes.button} style={{top: 15}}>
              Enviar
            </Button>
          </form>
        </Grid>

      <Grid item xs={7}>
      <Typography variant="h4" noWrap>Datos Almacenados:</Typography>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell>Apellidos</TableCell>
            <TableCell>Correo</TableCell>
            <TableCell>Contraseña</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {this.state.user.map((item) => {
                return (
                  <TableRow key={item}>
                    <TableCell>{item.nombre}</TableCell>
                    <TableCell>{item.apellidos}</TableCell>
                    <TableCell>{item.correo}</TableCell>
                    <TableCell>{item.password}</TableCell>
                  </TableRow>
                )
              })}
        </TableBody>
      </Table>
      </Grid>

      </Grid>
      </Paper>

      </div>
      </div>
    );
  }

}

Form.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Form);
