import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
  
});


function Nav() {

  return (
          <div className="Nav">

          <AppBar position="static">
            <Toolbar>
              <Typography variant="h6" color="inherit" >
                Inicio
              </Typography>
            </Toolbar>
          </AppBar>

          </div>
  );
}

Nav.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Nav);
