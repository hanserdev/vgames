import React, { Component } from 'react';
import Nav from './components/nav'
import Form from './components/form'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <Form />
      </div>
    );
  }
}

export default App;
