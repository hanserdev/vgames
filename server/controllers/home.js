var Model = require('../models/home')
var email = require('../modules/email')

var fs = require('fs');

module.exports = {
  envioDatos: envioDatos,
  datosReact: datosReact
}

function getJson(){
  return new Promise(function(resolve, reject) {
    resolve(fs.readFileSync(__dirname + '/datos.json', 'utf8'))
  });
}

function envioDatos(d) {
  return new Promise(async function (resolve, reject) {
    let datos = await getJson()
    console.log(datos);
    datos = JSON.parse(datos)
    resolve({err: false, res: datos})
  })
}

function datosReact(d) {
  return new Promise( async function (resolve, reject) {
    var user = {
      nombre: d.nombre,
      apellidos: d.apellidos,
      correo: d.correo,
      password: d.password
    }
    let datos = await getJson()
    datos = JSON.parse(datos)
    console.log(datos);
    datos.push(user)
    fs.writeFile(__dirname + '/datos.json', JSON.stringify(datos), 'utf8', (err) => {
      var to = d.correo
      var subject = 'Los datos fueron recibidos correctamente'
      var body =  `
      <h3>Hola ${d.nombre} ${d.apellidos}</h3>
      <b>Los datos fueron recibidos correctamente</b>
      <ul>
      <li>Correo: ${d.correo}</li>
      <li>Password: ${d.password}</li>
      </ul><br>
      <p>Este correo se genero de manera automatica, no responder al mismo, Gracias.</p>
      `
      var arr = []
      email.enviar(to, body, subject, arr)
      resolve({err: false, res: 'Los datos fueron recibidos correctamente'})
    })
  })
}

// function prueba(d) {
//   return new Promise(function (resolve, reject) {
//     Model.prueba(d)
//     .then(function (res) {
//       resolve(res)
//     })
//   })
// }
