var express = require('express');
var router = express.Router();
var ctrl = require('../controllers/home');

router.get('/datos', envioDatos)

router.post('/enviar_datos', datosReact)

function envioDatos(req, res) {
  ctrl.envioDatos()
  .then(function(result){
    res.json(result)
  })
}

function datosReact(req, res) {
  var d = req.body;
  ctrl.datosReact(d)
  .then(function(result){
    res.json(result)
  })
}

module.exports = router;
