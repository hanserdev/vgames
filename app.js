var express = require('express')
var cors = require('cors')
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser());
app.use(cookieParser());

var homeRoute = require("./server/routes/home");
app.use('/home', homeRoute);

var puerto = 3003

app.listen(puerto, function(){
  console.log('Escuchando en el puerto: ' + puerto);
})
